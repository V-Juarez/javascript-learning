Iniciar contenedor

```sh
docker-compose up -d postgres # name_container
```

Detener contenedor

```sh
docker-compose down postgres

# Para detener todos los contenedores
docker-compose down 
```

Ingresar por terminal a la base de datos

```sh
docker-compose exec postgres bash
```

Al ingresar al contenedor. Ingresar a la base de datos

```sh
:/# psql -h localhost -d my_store -U name_user
```


Integrar nodejs al proyecto

Instalar `pg`

```sh
npm install pg
```

Coneccion a la base de datos

```sh
npm install sequelize 
```

```sh
npm install --save pg pg-hstore
```

